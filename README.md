
## Set Up

* Fork/Clone
* Install dependencies - `npm install`
* Set mandrill api key in `.env` file
* Start app - `npm start`
* Start test - `npm test`

## Usage

* Open Postman for post your request data.
* Select type POST and given URL http://localhost:1337/send
* In Body tab select row and JSON(application/json)
* Copy and paste below data into the box. 
```
	{
	    "Body": {
            "Subject": "Test",
            "Name": "Md Maruf Rahman",
            "FromName":"DoNotReply",
            "From": "donotreply@aucorp.com",
            "To": "marufr@springrainit.com",
            "Message": "Test message"
        }
    }
  
```
* After send request get below output
```
  {
    "status_code": 0,
    "result": "Email send Successfully"
  }
  
```
