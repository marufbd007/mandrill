const Koa = require('koa');
const BodyParser = require('koa-bodyparser');
const Router = require('./App/Routes');

require('dotenv').config()
const App = new Koa();
const Port = process.env.PORT || 1337;
const AllError = require('koa-json-error');
function FormetError(Err) {
    return {
        Success: false,
        Message: Err.message,
    }
}
App.use(AllError(FormetError));
// body parser
App.use(BodyParser());
// routes
App.use(Router.routes());
App.use(Router.allowedMethods());


// server
const Server = App.listen(Port, () => {
  console.log(`Server listening on port: ${Port}`);
});

module.exports = Server;


