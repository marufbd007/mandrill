require('dotenv').config();
const Mandrill = require('node-mandrill')(process.env.EmailApiKey);
const _ = require('lodash');
let Chai = require("chai");
let ChaiHttp = require("chai-http");
var Server = require('../../index');
let Should = Chai.should();
Chai.use(ChaiHttp);
var Request = (Url,Data) => {
  return new Promise((resolve,reject)=>{
    Chai.request(Server).post(Url).send(Data).end(function (err, res) {
        if(err){
          reject(err);
        }
        resolve(res);
      });
  })
};

describe('Mandrill API', () => {
          it("for success response on /send POST\n\tresponse status is 200 and its a json data.\n\tresponse body is an object.\n\tresponse body has status_code and its value is 0.\n\tresponse body has result and its value is a string.", function(done) {
                let Data = {
                    "Body": {
                        "Subject": "Test",
                        "Name": "Md Maruf Rahman",
                        "FromName":"DoNotReply",
                        "From": "donotreply@aucorp.com",
                        "To": "marufr@springrainit.com",
                        "Message": "Test message"
                      }
                };
                Request('/send',Data).then((res)=>{
                      res.should.have.status(200);
                      res.should.be.json;
                      res.body.should.be.a('object');
                      res.body.should.have.property('status_code').with.equal(0);
                      res.body.should.have.property('result');
                      res.body.result.should.be.a('string');
                      done();
                  }).catch((err)=>{});
                  
            });
          it("for blank Body request on /send POST\n\tresponse status is 200 and its a json data.\n\tresponse body is an object.\n\tresponse body have status_code and its value is 111.\n\tresponse body has message, its a string and its value is 'Wrong Params!'.", function(done) {
            let Data = {};
            Request('/send',Data).then((res)=>{
                  res.should.have.status(200);
                  res.should.be.json;
                  res.body.should.be.a('object');
                  res.body.should.have.property('status_code').with.equal(111);
                  res.body.should.have.property('message');
                  res.body.message.should.be.a('string').with.equal('Wrong Params!');
                  done();
            }).catch((err)=>{}); 
          });

          it("for wrong Subject param request on /send POST\n\tresponse status is 200 and its a json data.\n\tresponse body is an object.\n\tresponse body have status_code and its value is 804.\n\tresponse body have error_message, its a string and its value is 'Email Subject is required!'.\n\tresponse body have result and its an empty string.", function(done) {
              let Data = {
                  "Body": {
                    "Subjectss": "Test",
                    "Name": "Md Maruf Rahman",
                    "FromName":"DoNotReply",
                    "From": "donotreply@aucorp.com",
                    "To": "marufr@springrainit.com",
                    "Message": "Test message"
                  }
              };
              Request('/send',Data).then((res)=>{
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.be.a('object');
                    res.body.should.have.property('status_code').with.equal(804);
                    res.body.should.have.property('error_message');
                    res.body.error_message.should.be.a('string').with.equal('Email Subject is required');
                    res.body.should.have.property('result');
                    res.body.result.should.be.a('string').with.equal('');
                    done();
              }).catch((err)=>{}); 
          });

          it("for wrong Message param request on /send POST\n\tresponse status is 200 and its a json data.\n\tresponse body is an object.\n\tresponse body have status_code and its value is 802.\n\tresponse body have error_message, its a string and its value is 'Message field is required'.\n\tresponse body have result and its an empty string.", function(done) {
            let Data = {
                "Body": {
                  "Subject": "Test",
                  "Name": "Md Maruf Rahman",
                  "FromName":"DoNotReply",
                  "From": "donotreply@aucorp.com",
                  "To": "marufr@springrainit.com",
                  "MessageSS": "Test message"
                }
            };
            Request('/send',Data).then((res)=>{
                  res.should.have.status(200);
                  res.should.be.json;
                  res.body.should.be.a('object');
                  res.body.should.have.property('status_code').with.equal(802);
                  res.body.should.have.property('error_message');
                  res.body.error_message.should.be.a('string').with.equal('Message field is required');
                  res.body.should.have.property('result');
                  res.body.result.should.be.a('string').with.equal('');
                  done();
            }).catch((err)=>{}); 
        });

        it("for wrong To param request on /send POST\n\tresponse status is 200 and its a json data.\n\tresponse body is an object.\n\tresponse body have status_code and its value is 801.\n\tresponse body have error_message, its a string and its value is 'To email address is required'.\n\tresponse body have result and its an empty string.", function(done) {
          let Data = {
              "Body": {
                "Subject": "Test",
                "Name": "Md Maruf Rahman",
                "FromName":"DoNotReply",
                "From": "donotreply@aucorp.com",
                "Toss": "marufr@springrainit.com",
                "Message": "Test message"
              }
          };
          Request('/send',Data).then((res)=>{
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('status_code').with.equal(801);
                res.body.should.have.property('error_message');
                res.body.error_message.should.be.a('string').with.equal('To email address is required');
                res.body.should.have.property('result');
                res.body.result.should.be.a('string').with.equal('');
                done();
          }).catch((err)=>{}); 
      });
});
