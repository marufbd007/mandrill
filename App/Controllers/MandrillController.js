const Validator = require('validatorjs');
require('dotenv').config();
const Mandrill = require('node-mandrill')(process.env.EmailApiKey);
const _ = require('lodash');
class MandrillController {
    async send() {
        try {
            let RequestData = {
                Subject: this.request.body.Body.Subject,
                Name: this.request.body.Body.Name,
                FromName: this.request.body.Body.FromName,
                From: this.request.body.Body.From,
                To: this.request.body.Body.To,
                Message: this.request.body.Body.Message
            };
            let Rules = {
                Subject: ['required', 'regex:/^[A-Za-z0-9\ ]+$/i'],
                Name: 'regex:/^[A-Za-z0-9\ ]+$/i',
                FromName: 'regex:/^[A-Za-z0-9\ ]+$/i',
                From: 'email',
                To: ['required', 'regex:/^([\\w+-.%]+@[\\w-.]+\\.[A-Za-z]{2,4},?)+$/'],
                Message: ['required', 'regex:/^[A-Za-z0-9\ ]+$/i']
            };

            let Messages = {
                required: ':attribute is required.',
                regex: ':attribute must contain alpha numeric only.',
                email: ':attribute must be a valid email address.',
            };

            let Validation = new Validator(RequestData, Rules, Messages);
            
            if( Validation.passes() === true && Validation.fails() === false ) {
                var Self = this;
                await MandrillController.Email(RequestData).then((response)=>{
                    Self.body = {
                        status_code:0,
                        result:"Email send Successfully"
                    }
               }).catch((err)=>{
                    Self.body = {
                        status_code: 500,
                        message: "API response error message!"    
                    }
               });
            }else{
                if(Validation.errors.first('Subject')){
                    this.body = {
                        status_code:804,
                        result:"",
                        error_message:"Email Subject is required"
                    }
                }
                else if(Validation.errors.first('Name')){
                    this.body = {
                        status_code:807,
                        result:"",
                        error_message:"Name must contain alphabets,numbers only."
                    }
                }
                else if(Validation.errors.first('FromName')){
                    this.body = {
                        status_code:806,
                        result:"",
                        error_message:"FromName must contain alphabets,numbers only."
                    }
                }
                else if(Validation.errors.first('From')){
                    this.body = {
                        status_code:805,
                        result:"",
                        error_message:"From must be a valid email address"
                    }
                }
                else if(Validation.errors.first('To')){
                    this.body = {
                        status_code:801,
                        result:"",
                        error_message:"To email address is required"
                    }
                }
                else if(Validation.errors.first('Message')){
                    this.body = {
                        status_code:802,
                        result:"",
                        error_message:"Message field is required"
                    }
                }
                else{
                    this.body = {
                        status_code: 222,
                        message: "Validation Error!",
                        ValidationErrors: validation.errors.all()
                    }
                } 
            }
        }catch(err){
            this.body = {
                status_code: 111,
                message: "Wrong Params!"
            }
        }
    }

    static Email(RequestData){
        return new Promise((resolve,reject)=>{
            try{
                 var EmailAddresss = _.split(RequestData.To, ',',RequestData.To.length);
                 var ToAddress = [];
                 if(EmailAddresss.length>0){
                     for (var i in EmailAddresss) {
                         ToAddress.push({
                             email: EmailAddresss[i]
                         });
                     }
                 }
                 Mandrill('/messages/send', {
                     message: {
                         to: ToAddress,
                         from_email: RequestData.From,
                         subject: RequestData.Subject,
                         text: RequestData.Message
                     }
                 }, function(error, response)
                 {
                     if (error){
                        reject(error);
                     }
                     resolve(response);
                 });
             }catch(err){
                 reject(err);
             }
        });
    }
}
module.exports = MandrillController;