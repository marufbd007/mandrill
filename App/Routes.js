// controller router
const Router = require('koa-controller-router');
const Path = require('path');
// midle wares
const authMiddleware = function *(next) {
    // auth check 
    
    yield next;
  };
  
  const isAdminMiddleware = function *(next) {
    // check user role 
    
    yield next;
  };
const router = new Router({
    controllersPath: Path.resolve(process.cwd(), 'app', 'Controllers')// default value is /path/to/project/controllers/ 
  });
  
  router.post('/send', 'MandrillController@send');

  module.exports = router;